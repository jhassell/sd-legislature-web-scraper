from bs4 import BeautifulSoup
import csv
import requests
from string import ascii_lowercase
from tqdm import tqdm
from shutil import copyfile

def create_http_session():
    """
    Quick little function for returning a requests.Session() instance
    with a properly set User-Agent header.
    :return: requests.Session()
    """
    session = requests.Session()
    session.headers.update({'User-Agent':
                            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
                            })
    return session


def soupify(session, url):
    """
    Makes parse-able HTML from any given URL.
    :param session: requests.Session()
    :param url: str
    :return: BeautifulSoup object
    """
    while True:
        try:
            r = session.get(url)
            break
        except Exception as e:
            print(e)
    return BeautifulSoup(r.content, 'html.parser')


class SDSenateScraper:
    def __init__(self):
        self.http_session = create_http_session()
        self.base_link = 'http://sdlegislature.gov/Legislative_Session/Committees/Default.aspx?Committee=361&Session=2017&tab=Agendas&document=none#'
        self.committees = {}

    def gather_committees(self):
        soup = soupify(self.http_session, self.base_link)
        committees = soup.find(
            'div',
            attrs={'id': 'ctl00_ContentPlaceHolder1_Committees_divCommittees'}
        )
        for link in committees.find_all('a'):
            self.committees[link.text.strip()] = \
                'http://sdlegislature.gov' + link.get('href')

    def scrape_data_for_committee(self, committee_name, committee_url):
        soup = soupify(self.http_session, committee_url)
        members_container = soup.find(
            'div', attrs={'id': 'ctl00_ContentPlaceHolder1_DetailContent'})
        labels = [i.text.replace(':', '')
                  for i in members_container.find_all('label')]
        values = [i.text.replace('\xa0', ' ')
                  for i in members_container.find_all('span')]
        data = dict(zip(labels, values))
        for key, value in data.items():
            if key in ['Chairs', 'Vice-Chair', 'Vice-Chairs']:
                fixed_name = value.split(',')
                data[key] = '{0} {1}'.format(
                    fixed_name[1].strip(), fixed_name[0].strip())
        with open('south_dakota_data.csv', 'a') as csvfile:
            csv_writer = csv.writer(csvfile)
            csvfile.write(committee_name)
            csvfile.write('\n')
            for key, value in data.items():
                if key in ['Chairs', 'Vice-Chair', 'Vice-Chairs']:
                    csv_writer.writerow([value, key])
                elif key in ['Representatives', 'Senators']:
                    values = value.split(';')
                    for value in values:
                        fixed_name = value.split()
                        fixed_name = "{0} {1}".format(
                            fixed_name[1], fixed_name[0].replace(',', ''))
                        csv_writer.writerow([fixed_name])
                elif key == 'Co-Chairs':
                    names = value.split('; ')
                    for name in names:
                        name = name.split(',')
                        fixed_name = "{0} {1}".format(
                            name[1], name[0].replace(',', ''))
                        position = key
                        csv_writer.writerow([fixed_name, position])
            csvfile.write('\n\n')

    def matrix_invert(self):
        member_soup = soupify(
            self.http_session,
            'http://sdlegislature.gov/Legislators/ContactLegislator.aspx?CurrentSession=True')
        members = ['http://sdlegislature.gov' +
                   i.get('href') for i in member_soup.find_all(
                       'a', text='View Profile')]
        for member in members:
            member_name, committees = self.member_handler(member)
            with open('matrix_invert.txt', 'a') as csvfile:
                csv_writer = csv.writer(csvfile)
                csv_writer.writerow([member_name, '~ '.join(committees)])

    def member_handler(self, member):
        soup = soupify(self.http_session, member)
        name = soup.find(
            'h3',
            attrs={'id': 'ctl00_ContentPlaceHolder1_hdMember'}).text.strip()
        name = name.split('—')[0]
        name = ' '.join(name.split()[1:])
        committees_table = soup.find(
            'table', attrs={'id': 'tblSessionCommitees'})
        committees = []
        for row in committees_table.find_all('tr')[1:]:
            cells = [i.text.strip() for i in row.find_all('td')]
            if cells[1] != 'Member':
                committees.append("{0} ({1})".format(cells[0], cells[1]))
            else:
                committees.append("{0}".format(cells[0]))
        return name, committees

def file_handler():
    with open('south_dakota_data.csv', 'w'):
        pass
    with open('south_dakota_data.txt', 'w'):
        pass
    with open('matrix_invert.txt', 'w'):
        pass


def main():
    file_handler()
    sds = SDSenateScraper()
    sds.gather_committees()
    for committee_name, committee_url in sds.committees.items():
        if committee_name in ['House of Representatives', 'Senate']:
            continue
        else:
            sds.scrape_data_for_committee(committee_name, committee_url)
    sds.matrix_invert()
    copyfile('south_dakota_data.csv', 'south_dakota_data.txt')



if __name__ == '__main__':
    main()
