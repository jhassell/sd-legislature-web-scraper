# README #

SD Legislative App Web Scraper.

### What is this repository for? ###

Scrapes web and puts SD House and Senate committee information into txt and app-spreadsheet format.

### How do I get set up? ###

Initialize virtual environment that contains modules you need.

workon py_3_leg

Then run it:
python south_dakota_web_scraper.py

Output is placed in the same directory as the above python file.

Note, pip freeze output:
beautifulsoup4==4.6.0
bs4==0.0.1
certifi==2017.11.5
chardet==3.0.4
Django==1.11.5
django-excel==0.0.9
django-extensions==1.9.1
django-shell-plus==1.1.7
et-xmlfile==1.0.1
html2text==2017.10.4
idna==2.6
jdcal==1.3
lml==0.0.1
mysqlclient==1.3.12
numpy==1.13.3
openpyxl==2.4.8
pandas==0.21.0
pyexcel==0.5.4
pyexcel-io==0.5.1
pyexcel-webio==0.1.2
pyexcel-xls==0.5.0
pyexcel-xlsx==0.5.0.1
python-dateutil==2.6.1
pytz==2017.2
requests==2.18.4
six==1.11.0
texttable==0.9.1
tqdm==4.19.4
urllib3==1.22
xlrd==1.1.0
xlwt==1.3.0
